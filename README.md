# vscode-remote-cloud-tools

Setup for using VS Code with container to work with AWS and GCP. Contains standard SDK and tools. 

## Setup

1. Install [Visual Studio Code](https://code.visualstudio.com/docs/setup/setup-overview) and [Docker Desktop](https://docs.docker.com/desktop/).
2. Install [Remote Development Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack).


## Use

1. Clone repository.
2. Open repository folder in VS Code.
3. The first time will take a lttle longer to build the container.

## Why use a container?

1. Repeatable environment that is usable across platform.
2. Immediately get update from colleagues, no need to repeat the same steps.
3. New team members do not have to repeat setup.
4. Do not mess up your local workstation with many installs.

Drawback
1. Use more disk space and memory.
 

## Reference
- [VS Code Remote Development](https://code.visualstudio.com/docs/remote/remote-overview)
- [vscode-dev-containers - python-3](https://github.com/microsoft/vscode-dev-containers/tree/master/containers/python-3)
- [aws-cli](https://github.com/aws/aws-cli/tree/v2)
- [AWS CLI Docker](https://github.com/aws/aws-cli/blob/v2/docker/Dockerfile)
